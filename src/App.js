import logo from './logo.svg';
import './App.css';
import InputSearch from './components/InputSearch';

function App() {
  return (

    <div className="App">
      <InputSearch textPlaceHolder="Ingrese el nombre..." classInput="contentInput" />
    </div>

  );
}

export default App;
