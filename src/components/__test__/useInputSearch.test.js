import useInputSearch from "../useInputSearch"
import { renderHook } from "@testing-library/react-hooks"


let useInputSearchResult;

describe('useInputSearch', () => {
    beforeEach(() => {
        useInputSearchResult = renderHook(() => useInputSearch.useActions())
    });

    it("Render correctly", () => {
        expect(useInputSearchResult.result.current).toBeDefined();
    })

    it("focusEvent,onBlurEvent ... are function", () => {

        expect(useInputSearchResult.result.current[0]).toEqual("");
        expect(useInputSearchResult.result.current[4]).toBeInstanceOf(Function); // focusEvent
        expect(useInputSearchResult.result.current[5]).toBeInstanceOf(Function); // onBludEvent
        expect(useInputSearchResult.result.current[6]).toBeInstanceOf(Function); // onKeyDownEvent
        expect(useInputSearchResult.result.current[7]).toBeInstanceOf(Function); // onChangeEvent

    })
   

})