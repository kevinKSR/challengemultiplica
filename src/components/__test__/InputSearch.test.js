import React from 'react';
import ReactDOM from 'react-dom';
import InputSearch from '../InputSearch';
import { render, fireEvent } from '@testing-library/react';


it('Render without collapse', () => {
    const div = document.createElement("div");
    ReactDOM.render(<InputSearch />, div);
})

it('Render inputSearch correctly', () => {

    const { getByTestId } = render(<InputSearch classInput="contentInput" />);
    expect(getByTestId('content')).toHaveClass("contentInput")

})

it('Render inputSearch correctly - 2', () => {

    const { getByTestId } = render(<InputSearch classInput="contentInputClass" />);
    expect(getByTestId('content')).toHaveClass("contentInputClass")

})

it('Render with value empty', () => {

    const { getByTestId } = render(<InputSearch classInput="contentInput" />);
    expect(getByTestId('input')).toHaveValue("") //vacio

})
