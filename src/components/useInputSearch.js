import axios from 'axios';
import { useEffect, useState } from 'react';

const useActions = () => {

    const [classFocus, setClassFocus] = useState('');
    const [listResult, setListResult] = useState([]);
    const [cursorCount, setCursorCount] = useState(0);
    const [textValue, setTextValue] = useState('');
    const [isLoader, setIsLoader] = useState(false);

    const focusEvent = () => {
        setClassFocus('svgFocus');
    }

    const onBlurEvent = () => {
        setClassFocus('')
    }

    const onKeyDownEvent = (event) => {
        switch (event.key) {
            case 'ArrowUp':
                cursorCount !== 0 && setCursorCount(cursorCount - 1)
                break;
            case 'ArrowDown':
                listResult.length - 1 !== cursorCount &&
                    setCursorCount(cursorCount + 1)
                break;
            case 'Enter':
                listResult.length > 0 && setTextValue(listResult[cursorCount].name);
                break;
            case 'Escape':
                setListResult([])
                break;
            default:
                break;
        }
    }


    const onChangeEvent = (event) => {
        const textVal = event.target.value;
        setTextValue(textVal)
        setListResult([]);
        setCursorCount(0);
        if (textVal.length > 1) {
            setIsLoader(true);
            setTimeout(() => { // simulator delay
                const listNames = JSON.parse(localStorage.getItem('listNames'));
                const result = listNames
                    .filter(_item => (_item.name).toLowerCase().includes(textVal.toLowerCase()))
                    .map((_item, _index) => {
                        _item.select = _index === 0 ? true : false;
                        return _item;
                    });
                setListResult(result)
                setIsLoader(false);
            }, 1000);
        }
    }


    const getNames = () => {
        return axios.get('https://6095a8ba116f3f00174b25ad.mockapi.io/names')
            .then(res => {
                localStorage.setItem('listNames', JSON.stringify(res.data))
            })
            .catch(err => console.log(err));
    };


    useEffect(() => {
        getNames();
    },[])


    useEffect(() => {
        const newResult = listResult.map((_item, _index) => {
            _index === cursorCount ? _item.select = true : _item.select = false
            return _item;
        });
        setListResult(newResult)
    }, [cursorCount])

    return [
        classFocus,
        listResult,
        textValue,
        isLoader,
        focusEvent,
        onBlurEvent,
        onKeyDownEvent,
        onChangeEvent
    ]

}

export default {
    useActions
}
