import React from 'react';
import './InputSearch.scss';
import { FiSearch, FiLoader } from 'react-icons/fi'
import useInputSearch from './useInputSearch';

const InputSearch = (props) => {
    const { textPlaceHolder, classInput } = props;

    const [

        classFocus,
        listResult,
        textValue,
        isLoader,
        focusEvent,
        onBlurEvent,
        onKeyDownEvent,
        onChangeEvent

    ] = useInputSearch.useActions();

    return (
        <div style={{ position: 'relative' }}>
            <div className={classInput} data-testid="content">
                <input
                    data-testid="input"
                    type="text"
                    placeholder={textPlaceHolder}
                    onFocus={focusEvent}
                    onBlur={onBlurEvent}
                    onChange={onChangeEvent}
                    onKeyDown={onKeyDownEvent}
                    value={textValue} />
                {
                    isLoader ?
                        <FiLoader size="25" className={classFocus} ></FiLoader>
                        :
                        <FiSearch size="25" className={classFocus} />
                }
            </div>
            {
                listResult.length > 0 &&
                <div className="contentResult">
                    {
                        listResult.map((_item, _index) => {
                            return (
                                <div className={`item ${_item.select && 'isSelected'}`} key={`item_${_item.id}`} >
                                    <li>{_item.name}</li>
                                </div>
                            )
                        })
                    }
                </div>
            }
        </div >

    )
}

export default InputSearch;
